/*
 *  Copyright (C) 2017  Andreas Jonsson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* eslint-env browser, amd, es6 */
((factory) => {
    "use strict";
    if (typeof define === "function" && define.amd) {
        define(factory);
    } else {
        factory();
    }
})(() => {
    "use strict";

    const defaults = {
        "pageSize": 15,
        "fetchContext": null,
        "end": null
    };

    function LazyArray (options) {

        const o = (n) => {
            if (typeof options[n] === "undefined") {
                if (typeof defaults[n] === "undefined") {
                    throw new Error("Missing mandatory option: " + n);
                }
                this[n] = defaults[n];
            } else {
                this[n] = options[n];
            }
        };

        o("pageSize");
        o("fetchData");
        o("fetchContext");
        o("end");

        this.ranges = [];
        this.buffers = [];

    }

    const p = LazyArray.prototype;

    p.setPageSize = function setPageSize (pageSize) {
        this.pageSize = pageSize;
    };

    p.setEnd = function setEnd (end) {
        this.end = end;
    };

    p.getNumPages = function getNumPages () {
        if (this.end !== null) {
            return Math.ceil((this.end + 1) / this.pageSize);
        }
        return null;
    };

    const _findGE = (a, x, p0, s, e) => {

        const dbl = p0 * 2;
        const x0 = a[dbl];
        const x1 = a[dbl + 1];
        if (x0 <= x && x <= x1) {
            return p0;
        }

        if (s >= e) {
            if (x0 > x) {
                return p0;
            }
            return p0 + 1;
        }

        let ne = null;
        let ns = null;

        if (x0 > x) {
            ns = s;
            ne = p0 === ns ? ns : p0 - 1;
        } else {
            ne = e;
            ns = p0 === ne ? ne : p0 + 1;
        }

        const np = Math.floor((ne - ns) / 2) + ns;

        return _findGE(a, x, np, ns, ne);
    };

    const findGE = (a, x) => {
        if (a.length === 0) {
            return 0;
        }
        const l = a.length / 2;
        return _findGE(a, x, Math.floor(l / 2), 0, l - 1);
    };

    const findHole = (a, ri, x, s) => {
        if (ri >= a.length) {
            return [s, x, ri, 0];
        }
        const x0 = a[ri];
        const x1 = a[ri + 1];
        if (s < x0) {
            return [s, Math.min(x, x0 - 1), ri, 0];
        }
        if (x <= x1) {
            return [];
        }
        return findHole(a, ri + 2, x, x1 + 1);
    };


    const continuous = (a, xs, xe) => {
        const ri = findGE(a, xs) * 2;
        return findHole(a, ri, xe, xs);
    };

    const callError = (error, message) => {
        if (typeof error === "function") {
            error(message);
        }
    };

    p._getData = function _getData (s, e, callback, errcb) {
        let i = findGE(this.ranges, s);
        const data = [];
        while (true) {
            const dbl = i * 2;
            const x0 = this.ranges[dbl];
            const x1 = this.ranges[dbl + 1];
            if (this.buffers.length <= i) {
                break;
            }
            const b = this.buffers[i];
            if (Array.isArray(b)) {
                const slice = b.slice(
                    Math.max(s - x0, 0),
                    Math.min(e, x1) - x0 + 1
                );
                for (let j = 0; j < slice.length; j += 1) {
                    data.push(slice[j]);
                }
                if (e <= x1) {
                    break;
                }
            } else {
                b.waiting.push(this._getData.bind(this, s, e, callback, errcb));
                b.error.push(errcb);
                return;
            }
            i += 1;
        }
        callback(data);
    };

    p.getPage = function getPage (page, callback, errcb, pageSize) {
        const ps = isJust(pageSize) ? pageSize : this.pageSize;
        const si = (page - 1) * ps;
        const n = page * ps;
        let ei = n - 1;

        if (this.end !== null) {
            if (si > this.end) {
                callback(null);
                return;
            }
            ei = Math.min(ei, this.end);
        }

        const hole = continuous(this.ranges, si, ei);

        if (hole.length === 0) {
            this._getData(si, ei, callback, errcb);
        } else {
            this._fill(
                hole,
                this.getPage.bind(this, page, callback, errcb, ps),
                errcb
            );
        }
    };

    p._setLock = function _setLock (h) {
        this.ranges.splice(h[2], 0, h[0], h[1]);
        this.buffers.splice([h[2] / 2], 0, {
            "waiting": [],
            "error": []
        });
    };

    p._removeLock = function _removeLock (h) {
        this.ranges.splice(h[2], 2);
        this.buffers.splice([h[2] / 2], 1);
    };

    p._fill = function _fill (hole, callback, error) {
        const [s, e] = hole;
        this._setLock(hole);
        const err = (msg) => {
            const _err = this.buffers[hole[2] / 2].error;
            this._removeLock(hole);
            _err.push(error);
            for (let i = 0; i < err.length; i += 1) {
                callError(_err[i], msg);
            }
        };
        const {waiting} = this.buffers[hole[2] / 2];
        const releaseWaiting = () => {
            waiting.push(callback);
            for (let i = 0; i < waiting.length; i += 1) {
                waiting[i]();
            }
        };

        this.fetchData.call(this.fetchContext, {
            "offset": s,
            "limit": e - s + 1,
            "callback": (data) => {
                const exp = e - s + 1;
                if (!Array.isArray(data) || data.length !== exp) {
                    if (!Array.isArray(data)) {
                        err("LazyArray: Did not receive an array of elements!");
                    } else if (data.length > exp) {
                        err("LazyArray: Too many elements in data array, expected " + exp + " was " + data.length);
                    } else {
                        this.end = s + data.length - 1;
                        if (data.length > 0) {
                            this.buffers[hole[2] / 2] = data;
                            this.ranges[hole[2] + 1] = this.end;
                            releaseWaiting();
                        }
                    }
                } else {
                    this.buffers[hole[2] / 2] = data;
                    releaseWaiting();
                }
            },
            "error": err.bind(null, "LazyArray: Fetch data failed!")
        });
    };

    return LazyArray;
});
